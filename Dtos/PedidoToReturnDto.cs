using System;
using System.Collections.Generic;
using RetroGames.API.Models;

namespace RetroGames.API.Dtos
{
    public class PedidoToReturnDto
    {
        public int Id { get; set; }
        public IEnumerable<LinpedForListDto> Lineas { get; set; }  
        public DateTime Date { get; set; }
        public string State { get; set; }
        public string Direccion { get; set; }
    }
}