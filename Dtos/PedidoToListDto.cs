using System;
using System.Collections.Generic;

namespace RetroGames.API.Dtos
{
    public class PedidoToListDto
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string State { get; set; }
        public string Direccion { get; set; }
        public ICollection<LinpedForListDto> Lineas { get; set; }  

    }
}