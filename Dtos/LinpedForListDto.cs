namespace RetroGames.API.Dtos
{
    public class LinpedForListDto
    {
        public int Id { get; set; }
        public decimal Importe { get; set; }
        public int Cantidad { get; set; }
        public int ProductId { get; set; }
        public string PhotoUrl { get; set; }
    }
}