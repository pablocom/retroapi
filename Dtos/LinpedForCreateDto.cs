using System.ComponentModel.DataAnnotations;

namespace RetroGames.API.Dtos
{
    public class LinpedForCreateDto
    {
        [Required]
        public int Cantidad { get; set; }
        [Required]
        public int IdProd { get; set; }
    }
}