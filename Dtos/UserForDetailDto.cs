using System;
using System.Collections.Generic;
using RetroGames.API.Models;

namespace RetroGames.API.Dtos
{
    public class UserForDetailDto
    {
        public int Id { get; set; }
        public string Username { get; set; } 
        public string Name { get; set; } 
        public string SecondName { get; set; } 
        public string Email { get; set; }
        public string Location { get; set; }
        public int RetroPoints { get; set; }
        public string Rol { get; set; }
        public DateTime Birthday { get; set; }
        public string Creditcard { get; set; } 
        public ICollection<Photo> Photos { get; set; }
    }
}