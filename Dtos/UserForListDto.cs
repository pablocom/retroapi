using System;

namespace RetroGames.API.Dtos
{
    public class UserForListDto
    {
        public int Id { get; set; }
        public string Username { get; set; } 
        public string Name { get; set; } 
        public string SecondName { get; set; } 
        public string Email { get; set; }
        public string Location { get; set; }
        public int RetroPoints { get; set; }
        public string Rol { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Creditcard { get; set; } 
        public string PhotoUrl { get; set; }
    }
}