using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RetroGames.API.Models;

namespace RetroGames.API.Dtos
{
    public class ProductForRegisterDto
    {  
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Brand { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public decimal PriceSecHand { get; set; }
        public ICollection<Photo> Photos { get; set; }
    }
}