using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RetroGames.API.Models;

namespace RetroGames.API.Dtos
{
    public class ProductForUpdateDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Brand { get; set; }
        public decimal Price { get; set; }
        public decimal PriceSecHand { get; set; }
        public int Stock { get; set; }
        public int StockSecHand { get; set; }
        public ICollection<Photo> Photos { get; set; }
    }
}