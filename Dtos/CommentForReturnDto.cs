namespace RetroGames.API.Dtos
{
    public class CommentForReturnDto
    {
        public string Text { get; set; }
        public int Value { get; set; }  
        public string Date { get; set; }
        public string UserName { get; set; }
    }
}