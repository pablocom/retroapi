namespace RetroGames.API.Dtos
{
    public class CommentForRegisterDto
    {
        public string Text { get; set; }
        public int Value { get; set; }
    }
}