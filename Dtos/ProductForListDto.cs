namespace RetroGames.API.Dtos
{
    public class ProductForListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Brand { get; set; }
        public decimal Price { get; set; }
        public decimal PriceSecHand { get; set; }
        public int Valuation { get; set; }
        public int Stock { get; set; }
        public int StockSecHand { get; set; }
        public string PhotoUrl { get; set; }
    }
}