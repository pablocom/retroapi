﻿CREATE TABLE IF NOT EXISTS `__EFMigrationsHistory` (
    `MigrationId` varchar(95) NOT NULL,
    `ProductVersion` varchar(32) NOT NULL,
    CONSTRAINT `PK___EFMigrationsHistory` PRIMARY KEY (`MigrationId`)
);

CREATE TABLE `Products` (
    `Id` int NOT NULL,
    `Name` longtext NULL,
    `Description` longtext NULL,
    `Brand` longtext NULL,
    `Price` decimal(65, 30) NOT NULL,
    `PriceSecHand` decimal(65, 30) NOT NULL,
    `Valuation` int NOT NULL,
    `Stock` int NOT NULL,
    `StockSecHand` int NOT NULL,
    CONSTRAINT `PK_Products` PRIMARY KEY (`Id`)
);

CREATE TABLE `Users` (
    `Id` int NOT NULL,
    `Username` longtext NULL,
    `Name` longtext NULL,
    `SecondName` longtext NULL,
    `Email` longtext NULL,
    `Location` longtext NULL,
    `RetroPoints` int NOT NULL,
    `Rol` longtext NULL,
    `Birthday` datetime(6) NOT NULL,
    `CreatedAt` datetime(6) NOT NULL,
    `PasswordHash` longblob NULL,
    `PasswordSalt` longblob NULL,
    `Creditcard` longtext NULL,
    CONSTRAINT `PK_Users` PRIMARY KEY (`Id`)
);

CREATE TABLE `Comments` (
    `Id` int NOT NULL,
    `Text` longtext NULL,
    `Value` int NOT NULL,
    `ProductId` int NOT NULL,
    `UserId` int NOT NULL,
    CONSTRAINT `PK_Comments` PRIMARY KEY (`Id`),
    CONSTRAINT `FK_Comments_Products_ProductId` FOREIGN KEY (`ProductId`) REFERENCES `Products` (`Id`) ON DELETE CASCADE,
    CONSTRAINT `FK_Comments_Users_UserId` FOREIGN KEY (`UserId`) REFERENCES `Users` (`Id`) ON DELETE CASCADE
);

CREATE TABLE `Photos` (
    `Id` int NOT NULL,
    `Url` longtext NULL,
    `Description` longtext NULL,
    `DateAdded` datetime(6) NOT NULL,
    `IsMain` bit NOT NULL,
    `ProductId` int NULL,
    `UserId` int NULL,
    CONSTRAINT `PK_Photos` PRIMARY KEY (`Id`),
    CONSTRAINT `FK_Photos_Products_ProductId` FOREIGN KEY (`ProductId`) REFERENCES `Products` (`Id`) ON DELETE RESTRICT,
    CONSTRAINT `FK_Photos_Users_UserId` FOREIGN KEY (`UserId`) REFERENCES `Users` (`Id`) ON DELETE RESTRICT
);

CREATE INDEX `IX_Comments_ProductId` ON `Comments` (`ProductId`);

CREATE INDEX `IX_Comments_UserId` ON `Comments` (`UserId`);

CREATE INDEX `IX_Photos_ProductId` ON `Photos` (`ProductId`);

CREATE INDEX `IX_Photos_UserId` ON `Photos` (`UserId`);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20190103115219_VamosQuePodemos', '2.1.4-rtm-31024');

ALTER TABLE `Comments` ADD `Date` datetime(6) NOT NULL DEFAULT '0001-01-01 00:00:00.000000';

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20190103173358_CommentDateAdded', '2.1.4-rtm-31024');

