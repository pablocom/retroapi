using System;
using System.Linq;
using AutoMapper;
using RetroGames.API.Dtos;
using RetroGames.API.Models;

namespace RetroGames.API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {   

            CreateMap<User, UserForListDto>()
                .ForMember(dest => dest.PhotoUrl, opt => {
                    // dentro de la collection del usuario, la foto que tenga IsMain a true
                    opt.MapFrom(src => src.Photos.FirstOrDefault(p => p.IsMain).Url);
                });

            CreateMap<Product, ProductForListDto>()
                .ForMember(dest => dest.PhotoUrl, opt => {
                    // dentro de la collection del usuario, la foto que tenga IsMain a true
                    opt.MapFrom(src => src.Photos.FirstOrDefault(p => p.IsMain).Url);
                });

            CreateMap<Photo, PhotosForDetailDto>();

            CreateMap<User, UserForDetailDto>()
                .ForMember(dest => dest.Birthday, opt => {
                    opt.MapFrom(src => ((DateTime)src.Birthday).ToShortDateString());
                });

            CreateMap<UserForUpdateDto, User>();
            CreateMap<ProductForUpdateDto, Product>();
            CreateMap<CommentForReturnDto, Comment>();
            CreateMap<PedidoToReturnDto, Pedido>();
            CreateMap<Linped, LinpedForListDto>()
                .ForMember(dest => dest.PhotoUrl, opt => {
                    opt.MapFrom(src => src.Product.Photos.FirstOrDefault(p => p.IsMain).Url);
                });
            CreateMap<Pedido, PedidoToListDto>();
        }
    }
}