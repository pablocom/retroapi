using System;
using System.Collections.Generic;

namespace RetroGames.API.Models
{
    public class Pedido
    {
        public int Id { get; set; }
        public ICollection<Linped> Lineas { get; set; }  
        public User User { get; set; }
        public DateTime Date { get; set; }
        public string State { get; set; }
        public string Direccion { get; set; }
    }
}