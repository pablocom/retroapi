namespace RetroGames.API.Models
{
    public class Linped
    {
        public int Id { get; set; }
        public decimal Importe { get; set; }
        public int Cantidad { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int PedidoId { get; set; }
        public Pedido Pedido { get; set; }
    }
}