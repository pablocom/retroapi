using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RetroGames.API.Models
{
    public class User
    {
      
        public int Id { get; set; }
        public string Username { get; set; } 
        public string Name { get; set; } 
        public string SecondName { get; set; } 
        public string Email { get; set; }
        public string Location { get; set; }
        public int RetroPoints { get; set; }
        public string Rol { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime CreatedAt { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string Creditcard { get; set; } 
        public ICollection<Photo> Photos { get; set; }
        public ICollection<Comment> Comment { get; set; }
        public ICollection<Pedido> Pedidos { get; set; }
    }
}