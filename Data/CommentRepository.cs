using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RetroGames.API.Models;

namespace RetroGames.API.Data
{
    public class CommentRepository : ICommentRepository
    {
        private readonly DataContext _context;

        public CommentRepository(DataContext context){
            _context = context;
        }

        public async Task<Comment> Create(Comment comment)
        {
            await _context.Comments.AddAsync(comment); // añadido prov a la db
            await _context.SaveChangesAsync();
            return comment;
        }

        public async Task<IEnumerable<Comment>> GetCommentsFromProduct(int productId)
        {
            var comments = _context.Comments
                            .Include(c => c.User)
                            .Include(c => c.Product)
                            .AsQueryable();
            comments = comments.Where(c => c.ProductId == productId);

            comments = comments.OrderByDescending(d => d.Date);

            return await comments.ToListAsync();
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }
        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}