using System.Collections.Generic;
using System.Threading.Tasks;
using RetroGames.API.Models;

namespace RetroGames.API.Data
{
    public interface IPedidoRepository
    {
        Task<IEnumerable<Pedido>> GetPedidos(int userId);
        Task<Pedido> GetPedido(int id);
        void Add<T>(T entity) where T: class;
        void Delete<T>(T entity) where T: class;
        Task<bool> SaveAll();
    }
}