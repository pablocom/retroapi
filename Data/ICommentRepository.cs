using System.Collections.Generic;
using System.Threading.Tasks;
using RetroGames.API.Models;

namespace RetroGames.API.Data
{
    public interface ICommentRepository 
    {
        Task<Comment> Create(Comment comment);
        Task<IEnumerable<Comment>> GetCommentsFromProduct(int ProductId);
        void Add<T>(T entity) where T: class;
        void Delete<T>(T entity) where T: class;
        Task<bool> SaveAll();
    }
}