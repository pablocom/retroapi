using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RetroGames.API.Models;

namespace RetroGames.API.Data
{
    public class ProductRepository : IProductRepository
    {
        private readonly DataContext _context;
        
        public ProductRepository(DataContext context){
            _context = context;
        }

        public async Task<IEnumerable<Product>> GetProducts() {
            var products = await _context.Products.Include(p => p.Photos).ToListAsync();
            return products;
        }

        public async Task<Product> Register(Product product)
        {
            await _context.Products.AddAsync(product);
            await _context.SaveChangesAsync();
            return product;
        }

        public async Task<bool> ProductExists(string productName) 
        {
            if(await _context.Products.AnyAsync(x => x.Name == productName))
                return true;
            
            return false;
        }

        public async Task<Product> GetProduct(int id)
        {
            var product = await _context.Products.Include(p => p.Photos).FirstOrDefaultAsync(u => u.Id == id);
            return product;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0; 
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }
    }
}