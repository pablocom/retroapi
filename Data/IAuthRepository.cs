using System.Threading.Tasks;
using RetroGames.API.Models;

namespace RetroGames.API.Data
{
    public interface IAuthRepository
    {
        Task<User> Register (User user, string password);
        Task<User> Login (string username, string password);
        Task<bool> UserExists(string username);
    }
}