using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using RetroGames.API.Models;

namespace  RetroGames.API.Data
{
    public class PedidoRepository : IPedidoRepository
    {

        private readonly DataContext _context;
        
        public PedidoRepository(DataContext context){
            _context = context;
        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<Pedido> GetPedido(int id)
        {
            var pedido = await _context.Pedidos.Include(p => p.User).Include(p => p.Lineas)
                                .ThenInclude(p => p.Product)
                                .ThenInclude(p => p.Photos).FirstOrDefaultAsync(p => p.Id == id);
            return pedido;
        }

        public async Task<IEnumerable<Pedido>> GetPedidos(int userId)
        {
            var pedidos = _context.Pedidos
                                    .Include(p => p.Lineas)
                                    .ThenInclude(p => p.Product)
                                    .ThenInclude(p => p.Photos)
                                    .Include(p => p.User)
                                    .AsQueryable();
            pedidos = pedidos.Where(p => p.State == "pagado" && p.User.Id == userId);

            pedidos = pedidos.OrderByDescending(d => d.Date);

            return await pedidos.ToListAsync();
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}