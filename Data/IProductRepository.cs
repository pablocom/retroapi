using System.Collections.Generic;
using System.Threading.Tasks;
using RetroGames.API.Models;

namespace RetroGames.API.Data
{
    public interface IProductRepository
    {
        Task<Product> Register(Product product);
        Task<bool> ProductExists(string productName);
        Task<IEnumerable<Product>> GetProducts();
        Task<Product> GetProduct(int id);
        Task<bool> SaveAll();
        void Add<T>(T entity) where T: class;
        void Delete<T>(T entity) where T: class;
    }
}