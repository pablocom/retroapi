using System;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RetroGames.API.Data;
using RetroGames.API.Dtos;
using RetroGames.API.Models;

namespace RetroGames.API.Controllers
{
    [Authorize]
    [Route("api/users/{userId}/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly IProductRepository _productRepo;
        private readonly IUserRepository _userRepo;
        private readonly ICommentRepository _repo;
        private readonly IMapper _mapper;

        public CommentController(IProductRepository productRepo, 
                                IUserRepository userRepo, 
                                ICommentRepository repo, IMapper mapper) {
            _productRepo = productRepo;
            _userRepo = userRepo;
            _repo = repo;
            _mapper = mapper;
        }

        [HttpPost("{productId}")]
        public async Task<IActionResult> UploadComment (int productId, int userId, CommentForRegisterDto commentForRegisterDto) {
            var user = await _userRepo.GetUser(userId);    
            
            if (user.Id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value)) {
                return Unauthorized();
            }
            
            var product = await _productRepo.GetProduct(productId);

            if (product == null) {
                return NotFound(new { error = "No se ha encontrado el producto" });
            }

            var comment = new Comment {
                Text = commentForRegisterDto.Text,
                Value = commentForRegisterDto.Value,
                Date = DateTime.Now,
                UserId = userId,
                ProductId = productId
            };

            var commentForReturn = new { text = commentForRegisterDto.Text, Value = commentForRegisterDto.Value };

            _repo.Add(comment);

            if (await _repo.SaveAll()) {
                return Ok(new { message = "Se ha creado correctamente el comentario", coment = commentForReturn });
            }

            throw new Exception("Ha ocurrido algo en la llamada de crear comentario");
        }
    }
}