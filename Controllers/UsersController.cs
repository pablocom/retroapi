using AutoMapper;
using RetroGames.API.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using RetroGames.API.Dtos;
using System.Security.Claims;
using System;

namespace RetroGames.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _repo;
        private readonly IMapper _mapper;

        public UsersController(IUserRepository repo, IMapper mapper){
            _repo = repo;
            _mapper = mapper;
        }

        // GET /api/users
        [HttpGet]
        public async Task<IActionResult> GetUsers() {
            var adminId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var adminFromRepo = await _repo.GetUser(adminId);
            
            if(adminFromRepo == null)
                return BadRequest(new { error = "No se ha encontrado el administrador especificado" });

            if(adminFromRepo.Rol != "admin")
                return Unauthorized();

            var users = await _repo.GetUsers();

            var usersToReturn = _mapper.Map<IEnumerable<UserForListDto>>(users);

            return Ok(usersToReturn);
        }
        // GET: /api/users/2
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id) {
            if (id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value)) 
                return Unauthorized(); // si el usuario no coincide con el token

            var user = await _repo.GetUser(id);

            var userToReturn = _mapper.Map<UserForDetailDto>(user);

            return Ok(userToReturn);
        }

        // PUT: /api/users/update-user/2 
        [HttpPut("update-user/{id}")]
        public async Task<IActionResult> UpdateUser(int id, UserForUpdateDto userForUpdateDto) {

            if (id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value)) 
                return Unauthorized(); // si el usuario no coincide con el token

            var userFromRepo = await _repo.GetUser(id);

            if(userFromRepo == null)
                return NotFound(new { error = "No se ha encontrado un usuario con esa id"} );

            _mapper.Map(userForUpdateDto, userFromRepo);

            if (await _repo.SaveAll()) 
                return Ok(new { message = "Usuario modificado con éxito"});

            return BadRequest(new { error = "No se ha detectado ningún cambio en el usuario"});
        }

        // DELETE /api/users/delete-user/{id}
        [HttpDelete("delete-user/{id}")]
        public async Task<IActionResult> DeleteUser(int id) {
            if (id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value)) 
                return Unauthorized();

            var userFromRepo = await _repo.GetUser(id);

            if (userFromRepo == null) 
                return NotFound(new { error = "No existe ningún usuario con ese id"});
            
            _repo.Delete(userFromRepo);

            if(await _repo.SaveAll()) {
                return Ok(new { message = "El usuario ha sido eliminado correctamente" });
            }

            return BadRequest("Failed to delete de photo");
        }
    }
}