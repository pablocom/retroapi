using Microsoft.AspNetCore.Mvc;
using RetroGames.API.Data;
using System.Threading.Tasks;
using RetroGames.API.Dtos;
using RetroGames.API.Models;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using System.Collections.Generic;
using System.Security.Claims;

namespace RetroGames.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductRepository _repo;
        private readonly IUserRepository _repouser;
        private readonly IMapper _mapper;
        private readonly ICommentRepository _repocomment;

        public ProductsController(IProductRepository repo, IMapper mapper, IUserRepository repouser, ICommentRepository repocomment){
            _repo = repo;
            _mapper = mapper;        
            _repouser = repouser;
            _repocomment = repocomment;
        }

        // GET api/products
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetProducts() {
            var products = await _repo.GetProducts();

            var productsToReturn = _mapper.Map<IEnumerable<ProductForListDto>>(products);

            return Ok(productsToReturn);
        }

        // POST api/products/subir-producto
        [HttpPost("subir-producto")]
        public async Task<IActionResult> Register(ProductForRegisterDto productForRegisterDto){

            if (await _repo.ProductExists(productForRegisterDto.Name))
                return BadRequest(new { error = "Product already exists" } );

            var productToCreate = new Product {
                Name = productForRegisterDto.Name,
                Description = productForRegisterDto.Description,
                Brand = productForRegisterDto.Brand,
                Price = productForRegisterDto.Price,
                PriceSecHand = productForRegisterDto.PriceSecHand,
                Stock = 1,
                StockSecHand = 0,
                Photos = productForRegisterDto.Photos
            };

            var createdProduct = await _repo.Register(productToCreate);

            return StatusCode(201, new {createdProduct = createdProduct}); 
        }

        // GET /api/products/1
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetProduct(int id){
            var productFromRepo = await _repo.GetProduct(id);

            if (productFromRepo == null) 
                return NotFound(new { 
                    error = "No se ha encontrado el producto, quizás nuestro buscador le sirva de ayuda" 
                });
            
            return Ok(productFromRepo);
        }

        // POST /api/products/editar-producto/2
        [HttpPut("editar-producto/{id}")]
        public async Task<IActionResult> EditProduct(ProductForUpdateDto productForUpdateDto, int id) {

            // buscamos ese usuario en el backend y comprobamos que el rol es admin
            var userUpdating = await _repouser.GetUser(int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value));
            if (userUpdating.Rol != "admin")
                return Unauthorized();

            // nos quedamos con el producto que queremos editar en la variable productFromRepo
            var productFromRepo = await _repo.GetProduct(id);
            if (productFromRepo == null)
                return NotFound(new { error = "No se ha encontrado el producto" });

            // el mapper se encarga que transformar un objeto en otro viendo las propiedades que coinciden
            _mapper.Map(productForUpdateDto, productFromRepo);

            // esto guarda los cambios que hayamos hecho en las entidades dentro de la base de datos
            if (await _repo.SaveAll()) 
                return Ok(new { message = "Se ha modificado el producto" });

            // si se llega aqui se lanza una excepcion error 500 internal server error
            return BadRequest(new { error = "No se ha detectado ningún cambio en el producto"});
        }

        // DELETE /api/products/delete-product/{id}
        [HttpDelete("delete-product/{id}")]
        public async Task<IActionResult> DeleteProduct(int id) {
            var adminId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var admin = await _repouser.GetUser(adminId);
            if (admin == null)
                return BadRequest(new { error = "No se ha encontrado el admin especificado" });

            if (admin.Rol != "admin")
                return Unauthorized(); 

            var productFromRepo = await _repo.GetProduct(id);
 
            if (productFromRepo == null) 
                return NotFound(new { error = "No existe ningún producto con ese id" });
            
            _repo.Delete(productFromRepo);

            if(await _repo.SaveAll()) {
                return Ok(new { message = "El producto ha sido eliminado correctamente" });
            }

            return BadRequest("Failed to delete de product");
        }

        [HttpGet("get-comments/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetComments(int id) {

            var product = await _repo.GetProduct(id);

            if (product == null)
                return NotFound(new {error = "No se ha encontrado el producto especificado"});

            var commentsFromRepo = await _repocomment.GetCommentsFromProduct(id);

            if (commentsFromRepo == null) 
                return NotFound();

            var comments = _mapper.Map<IEnumerable<CommentForReturnDto>>(commentsFromRepo);

            return Ok(comments);

            throw new System.Exception("Error en la llamada get comentarios del producto");
        }
    }
}