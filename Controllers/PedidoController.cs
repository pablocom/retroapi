using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RetroGames.API.Data;
using RetroGames.API.Dtos;
using RetroGames.API.Models;

namespace RetroGames.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PedidoController : ControllerBase
    {
        private readonly IPedidoRepository _repo;
        private readonly IUserRepository _repouser;
        private readonly IProductRepository _repoproduct;
        private readonly IMapper _mapper;

        public PedidoController (IPedidoRepository repo, IUserRepository repouser, IMapper mapper, IProductRepository repoproduct){
            _repo = repo;
            _mapper = mapper;
            _repouser = repouser;
            _repoproduct = repoproduct;
        }

        // GET /api/pedido/get-pedidos
        [HttpGet("get-pedidos/todos")]
        public async Task<IActionResult> GetPedidos() {
            var pedidos = await _repo.GetPedidos(int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value));
            
            if (pedidos == null)
                return NotFound(new { error = "No se han encontrado pedidos para ese usuario"});

            var pedidosToReturn = _mapper.Map<IEnumerable<PedidoToListDto>>(pedidos);

            return Ok(new { pedidosToReturn });
        }

        // GET /api/pedido/1
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPedido(int id) {
            var pedido = await _repo.GetPedido(id);

            if (pedido == null) 
                return NotFound(new { error = "No se ha encontrado ningun pedido con el Id especificado" });

            if (pedido.User.Id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var pedidoToReturn = _mapper.Map<PedidoToReturnDto>(pedido);

            pedidoToReturn.Lineas = _mapper.Map<IEnumerable<LinpedForListDto>>(pedidoToReturn.Lineas);

            return Ok(new { pedido = pedidoToReturn });
        }

        // POST /api/pedido/create-pedido
        [HttpPost("create-pedido")]
        public async Task<IActionResult> CrearPedido(LinpedForCreateDto linpedForCreate) {
            var idUser = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var user = await _repouser.GetUser(idUser);
            
            if (user == null) 
                return Unauthorized();
            
            var pedidoToCreate = new Pedido {
                User = user,
                Date = DateTime.Now,
                State = "pendiente"
            };

            _repo.Add(pedidoToCreate);

            if (await _repo.SaveAll() == false) {
                return BadRequest(new { error = "Algo ha salido mal creando el pedido" });
            }

            var product = await _repoproduct.GetProduct(linpedForCreate.IdProd);

            if (product == null)
                return BadRequest(new { error = "El id del producto no se corresponde con ningun producto en la base de datos"});
            
            var importeLinea = product.Price * linpedForCreate.Cantidad;

            var linpedToCreate = new Linped {
                Importe = importeLinea,
                Cantidad = linpedForCreate.Cantidad,
                ProductId = product.Id,
                PedidoId = pedidoToCreate.Id
            };

            _repo.Add(linpedToCreate);

            if (await _repo.SaveAll() == false) {
                return BadRequest(new { error = "Algo ha salido mal creando el pedido" });
            }

            var pedidoToReturn = _mapper.Map<PedidoToReturnDto>(pedidoToCreate);

            pedidoToReturn.Lineas = _mapper.Map<IEnumerable<LinpedForListDto>>(pedidoToReturn.Lineas); 

            return Ok(new { message = "El pedido ha sido creado correctamente", pedido = pedidoToReturn });
        }

        // POST /api/pedido/anade-pedido
        [HttpPost("anade-pedido/{id}")]
        public async Task<IActionResult> AnadePedido(int id, LinpedForCreateDto linpedForCreate) {
            var pedido = await _repo.GetPedido(id);

            if (pedido == null)
                return NotFound(new { error = "No se ha encontrado ninguna cesta con ese Id"});

            if (pedido.State == "pagado")
                return BadRequest(new {error = "La cesta a la que se esta intentando añadir el pedido esta finalizada"});

            var product = await _repoproduct.GetProduct(linpedForCreate.IdProd);

            if (product == null)
                return BadRequest(new { error = "El id del producto no se corresponde con ningun producto en la base de datos"});
            
            var importeLinea = product.Price * linpedForCreate.Cantidad;

            var linpedToCreate = new Linped {
                Importe = importeLinea,
                Cantidad = linpedForCreate.Cantidad,
                ProductId = product.Id,
                PedidoId = pedido.Id
            };

            _repo.Add(linpedToCreate);

            if (await _repo.SaveAll() == false) 
                return BadRequest(new { error = "Algo ha salido mal creando el pedido" });

            var pedidoToReturn = _mapper.Map<PedidoToReturnDto>(pedido);

            pedidoToReturn.Lineas = _mapper.Map<IEnumerable<LinpedForListDto>>(pedidoToReturn.Lineas); 

            return Ok(new { message = "Se ha anadido el producto correctamente a la cesta", pedido = pedidoToReturn});
        }

        [HttpPost("pagar-pedido/{id}")]
        public async Task<IActionResult> PagarPedido(int id) {

            var pedido = await _repo.GetPedido(id);

            if (pedido == null)
                return NotFound(new { error = "No se ha encontrado ningun pedido con el Id Especificado"});

            if (pedido.State == "pagado")
                return BadRequest( new {error ="El pedido que estas intentando finalizar ya ha sido pagado"} );

            pedido.State = "pagado";

            if(await _repo.SaveAll())
                return Ok(new { message = "Pedido pagado con éxito"});

            return Ok();
        }


    }
}