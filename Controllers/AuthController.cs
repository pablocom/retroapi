using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using RetroGames.API.Data;
using RetroGames.API.Dtos;
using RetroGames.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Globalization;

namespace RetroGames.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository _repo;
        private readonly IConfiguration _config;
        public AuthController(IAuthRepository repo, IConfiguration config)
        {
            _config = config;
            _repo = repo;
        }

        // POST /api/auth/register
        [HttpPost("register")]
        public async Task<IActionResult> Register(UserForRegisterDto userForRegisterDto)
        {
            // validate request

            userForRegisterDto.Username = userForRegisterDto.Username.ToLower();
            if (await _repo.UserExists(userForRegisterDto.Username))
                return BadRequest(new { error = "User already exists" } );
                // return BadRequest("User already exists");

            var BirthdayAux = DateTime.ParseExact(userForRegisterDto.Birthday, "M/d/yyyy", CultureInfo.InvariantCulture);

            var userToCreate = new User {
                Username = userForRegisterDto.Username,
                Name = userForRegisterDto.Name,
                SecondName = userForRegisterDto.SecondName,
                Email = userForRegisterDto.Email,
                Location = userForRegisterDto.Location,
                RetroPoints = 0,
                Rol = userForRegisterDto.Rol,
                CreatedAt = DateTime.UtcNow,
                Creditcard = null,
                Birthday = BirthdayAux
            };

            var createdUser = await _repo.Register(userToCreate, userForRegisterDto.Password);

            return StatusCode(201, new { createdUser = createdUser } );
            // return Ok(createdUser);
        }

        // POST /api/auth/login
        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLoginDto userForLoginDto)
        {
            // Vemos si hay un usuario que coincida en nuestra bas de datos
            var userFromRepo = await _repo.Login(userForLoginDto.Username.ToLower(), userForLoginDto.Password);

            if (userFromRepo == null) return NotFound(new { error = "Usuario y/o contraseña incorrectos" }); // si userFromRepo es null, no 

            // si existe tenemos que generar el jwt con el que van a hacer las consultas
            //
            var claims = new[] {
                new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),
                new Claim(ClaimTypes.Name, userFromRepo.Username)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8
                        .GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return Ok(new {
                token = tokenHandler.WriteToken(token),
                user = userFromRepo
            });
        }
    }
}